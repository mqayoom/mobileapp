/**
 * @license
 * Copyright (c) 2014, 2019, Oracle and/or its affiliates.
 * The Universal Permissive License (UPL), Version 1.0
 * @ignore
 */
/**
 * Copyright (c) 2014, 2019, Oracle and/or its affiliates.
 * The Universal Permissive License (UPL), Version 1.0
 */
'use strict';
define(['appUtils',
        'ojs/ojknockout-keyset',
        'ojs/ojarraydataprovider',
        'ojs/ojknockout',
        'ojs/ojlistview'],
  function(appUtils, KeySet, ArrayDataProvider) {
    function aboutList(params) {
      var self = this;
      self.optionChange = params.optionChange;
      // retrieve about items to render the list
      self.aboutOptions = new ArrayDataProvider(params.list, {keyAttributes: 'id'});
      self.selectedItem = new KeySet.ObservableKeySet();
      self.prefetch = function() {
        self.selectedItem.clear();
      }
      self.transitionCompleted = function() {
        appUtils.setFocusAfterModuleLoad('startBtn');
      }
    }
    return aboutList;
  });